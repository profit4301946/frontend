const getBigImage = async () => {

    const imgCol = $('.big-image')
    imgCol.empty()

    const url = 'http://' + '93.95.97.162:80' + '/photo/' + localStorage.getItem('login')

    const request = await fetch(url, {
        headers: {
            'Access-Control-Allow-Origin': 'http://' + '93.95.97.162:80' + '/',
            "Accept": "application/json", "Content-Type": "application/json"
        }
    })

    const img = await request.json()

    imgCol.append(
        `
        <img class="img-fluid" src="${img}" alt="big avatar" width="300px"></img>
        `
    )
}

const getInfoAboutProfile = async () => {

        const myHobbies = $('.all-hobbies')

        //Очистить список хобби
        myHobbies.empty()

        // let url = 'http://' + fast_api_host + '/me'

        // let request = await fetch(url, {
        //     headers: {
        //         'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
        //         "Accept": "application/json", "Content-Type": "application/json"
        //     }
        // })

        // let response = await request.json()

        // const hobbiesIds = response.hobby

        const hobbiesIds = localStorage.getItem('hobbies')

        //const hobbiesIds = localStorage.getItem('hobbies')



            url = 'http://' + fast_api_host + '/hobby/' + hobbiesIds

            request = await fetch(url, {
                headers: {
                    'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
                    "Accept": "application/json", "Content-Type": "application/json"
                }
            })
        
            let hobby = await request.json()

            myHobbies.append(
                `
                <div class="hobby new-hobby-reg mr-20" >
                    <div class="hobbyId" hidden>${hobby.id}</div>
                    <p class="">${hobby.name}</p>
                </div>
                `
            )

    
}

$('.fio').text(localStorage.getItem('login'))

//getBigImage()

getInfoAboutProfile()