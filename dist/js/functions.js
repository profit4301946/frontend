const fast_api_host = '93.95.97.162:80'

const getNewHobbies = async () => {
    
    const myHobbies = $('.new-hobbies')

    myHobbies.empty()

    //Взять из бэка хобби

    const url = 'http://' + fast_api_host + '/hobby'

    const request = await fetch(url, {
        headers: {
            'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
            "Accept": "application/json", "Content-Type": "application/json"
        }
    })

    const hobbies = await request.json()

    hobbies.forEach(hobby => {
        myHobbies.append(
            `
            <div class="hobby new-hobby-reg mr-20" onclick="showNewHobbyInfo(this)">
                <div class="hobbyId" hidden>${hobby.id}</div>
                <p class="">${hobby.name}</p>
            </div>
            `
        )
    }) 
}

const getUserAndHisHobbies = async () => {

    const myHobbies = $('.my-hobby-list')

    //Очистить список хобби
    myHobbies.empty()

    // let url = 'http://' + fast_api_host + '/me'

    // let request = await fetch(url, {
    //     headers: {
    //         'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
    //         "Accept": "application/json", "Content-Type": "application/json"
    //     }
    // })

    // let response = await request.json()

    // console.log(response)
    // console.log('response: '+response)

    // const hobbiesIds = response.hobby

    const hobbiesIds = localStorage.getItem('hobbies')

    console.log(hobbiesIds)


        url = 'http://' + fast_api_host + '/hobby/' + hobbiesIds

        request = await fetch(url, {
            headers: {
                'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
                "Accept": "application/json", "Content-Type": "application/json"
            }
        })
    
        let hobby = await request.json()

        // url = 'http://' + fast_api_host + '/group/' + id

        // request = await fetch(url, {
        //     headers: {
        //         'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
        //         "Accept": "application/json", "Content-Type": "application/json"
        //     }
        // })
    
        // let users = await request.json()

        // let photos = ''

        // users.forEach(async nick => {

        //     url = 'http://' + fast_api_host + '/photo/' + nick

        //     request = await fetch(url, {
        //         headers: {
        //             'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
        //             "Accept": "application/json", "Content-Type": "application/json"
        //         }
        //     })

        //     let photo = await request.json()

        //     photos += `
        //     <div class="col-2 d-flex justify-content-center align-items-center">
        //         <a class="text-decoration-none" href="">
        //             <img class="img-fluid" src="${photo}" alt="avatar" width="70px">
        //         </a>
        //     </div>
        //     `

        myHobbies.append(
            `
            <div class="row my-hobby mx-1">
                <div class="col-4 d-flex justify-content-center align-items-center">
                    <div class="big-hobby-img">
                        <img class="" src="images/hobby_ava.png" alt="big avatar" width="200px">
                    </div>
                    
                </div>
                <div class="col-8 p-4">
                    <h3 class="my-hobby-name">${hobby.name}</h3>
                    <div class="my-hobby-desc">
                        <p>${hobby.discription}</p>
                    </div>
                    <div class="row people-of-hobby">
                        
                    </div>
                    <div class="btns d-flex">
                        <a href="${hobby.chat_id}"><button class="btn btn-primary mr-20">Вступить в группу</button></a>
                        <button class="btn btn-outline-primary">Найти приятеля</button>
                    </div>
                </div>
            </div>
            `
        )
    


}

const showNewHobbyInfo = async (el) => {

    //Подгрузить из бэка инфу про хобби по el.id

    const hobbyId = $('.hobbyId', el)

    let url = 'http://' + fast_api_host + '/hobby/' + hobbyId.text()

    let request = await fetch(url, {
        headers: {
            'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
            "Accept": "application/json", "Content-Type": "application/json"
        }
    })
    
        let hobby = await request.json()

        url = 'http://' + fast_api_host + '/group/' + hobbyId.text()

        request = await fetch(url, {
            headers: {
                'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
                "Accept": "application/json", "Content-Type": "application/json"
            }
        })
    
        let users = await request.json()

        let photos = ''

        users.forEach(async nick => {

            url = 'http://' + fast_api_host + '/photo/' + nick

            request = await fetch(url, {
                headers: {
                    'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
                    "Accept": "application/json", "Content-Type": "application/json"
                }
            })

            let photo = await request.json()

            photos += `
            <div class="col-2 d-flex justify-content-center align-items-center">
                <a class="text-decoration-none" href="">
                    <img class="img-fluid" src="${photo}" alt="avatar" width="70px">
                </a>
            </div>
            `

        })

    const newHobbyInfo = $('.new-hobby-info')  

    newHobbyInfo.html(
        `
        <div class="row new-hobby-info mx-1 mt-3">
            <div class="col-4 d-flex justify-content-center align-items-center">
                <div class="big-hobby-img">
                    <img class="" src="images/hobby_ava.png" alt="big avatar" width="200px">
                </div>
                
            </div>
            <div class="col-8 p-4">
                <h3 class="my-hobby-name">${hobby.name}</h3>
                <div class="my-hobby-desc">
                    <p>${hobby.discription}</p>
                </div>
                <div class="row people-of-hobby">
                ${photos}
                </div>
                <div class="btns d-flex">
                    <a href="${hobby.chat_id}"><button class="btn btn-primary mr-20">Вступить в группу</button></a>
                    <button class="btn btn-outline-primary">Найти приятеля</button>
                </div>
            </div>
        </div>
        `
    )

    if (newHobbyInfo.is(':hidden')){
        newHobbyInfo.removeAttr('hidden')
    }

}


const clickedHobby = (hobby) => {

    if (hobby.hasAttribute('selected')){
        hobby.removeAttribute('selected')
    }
    else{
        hobby.setAttribute('selected', true)
    }
}


const loginPost = () => {

    const url = 'http://' + fast_api_host + '/login'
    
    const nick = $('#nick').val()
    const code = $('#code').val()

    const data = {
        'tg_login': nick,
        'password' : code
    }

    $.ajax({
          type: 'POST',
          url: url,
          data: JSON.stringify(data),
          dataType: "json",
          headers: {
            'Access-Control-Allow-Origin': 'http://' + fast_api_host + '/',
            "Accept": "application/json", "Content-Type": "application/json"
         },
          success: (data) => {
            localStorage.setItem('login', nick);
            location.href = './registerProfile.html'
          },
          error: (error) => {
            $('.alert').removeAttr('hidden')
          }
    });

}