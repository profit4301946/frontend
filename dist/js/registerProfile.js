const getNewHobbiesForRegisterProfile = async () => {
    
    const myHobbies = $('.all-hobbies')

    myHobbies.empty()

    //Взять из бэка хобби

    const url = 'http://' + '93.95.97.162:80' + '/hobby'

    const request = await fetch(url, {
        headers: {
            'Access-Control-Allow-Origin': 'http://' + '93.95.97.162:80' + '/',
            "Accept": "application/json", "Content-Type": "application/json"
        }
    })

    const hobbies = await request.json()

    hobbies.forEach(hobby => {
        myHobbies.append(
            `
            <div class="hobby new-hobby-reg mr-20" onclick="clickedHobby(this)">
                <div class="hobbyId" hidden>${hobby.id}</div>
                <p class="">${hobby.name}</p>
            </div>
            `
        )
    }) 
}

const aboutPost = (btn) =>{

    btn.disabled = true
    
    const about = $('#about').val()

    const data = about

    let url = 'http://' + '93.95.97.162:80' + '/aboutme?dis=' + data

    console.log('Data about '+data)

    $.ajax({
          type: 'POST',
          url: url,
        //   data: data,
          dataType: "json",
          headers: {
            'Access-Control-Allow-Origin': 'http://' + '93.95.97.162:80' + '/',
            "Accept": "application/json", "Content-Type": "application/json"
         },
          success: (data) => {
            //location.replace('./index.html')
          },
          error: (error) => {
            //$('.alert').removeAttr('hidden')
          }
    });

  

    const hobbies = $('.all-hobbies > .hobby')


    hobbies.each((i, val) => {

        if (val.hasAttribute('selected')){
            
            let hobbyId = $('.hobbyId', val).text()

            url = 'http://' + '93.95.97.162:80' + '/addhobby?id_hob=' + hobbyId
            
            localStorage.setItem('hobbies', hobbyId)

            $.ajax({
                type: 'POST',
                url: url,
                // data: hobbyId,
                dataType: "json",
                headers: {
                  'Access-Control-Allow-Origin': 'http://' + '93.95.97.162:80' + '/',
                  "Accept": "application/json", "Content-Type": "application/json"
               },
                success: (data) => {
                  //location.replace('./index.html')
                },
                error: (error) => {
                  //$('.alert').removeAttr('hidden')
                }
          });

        }
        
    })

    setTimeout(()=>{
        location.href = './home.html'
    }, 2000)

    
}

const getBigImage = async () => {

    const imgCol = $('.big-image')
    imgCol.empty()

    const url = 'http://' + '93.95.97.162:80' + '/photo/' + localStorage.getItem('login')

    const request = await fetch(url, {
        headers: {
            'content-type': 'multipart/form-data',
            'Access-Control-Allow-Origin': 'http://' + '93.95.97.162:80' + '/',
            "Accept": "application/json", "Content-Type": "application/json"
        }
    })

    const img = await request.json()

    imgCol.append(
        `
        <img class="img-fluid" src="${img}" alt="big avatar" width="300px"></img>
        `
    )
}

$('.fio').text(localStorage.getItem('login'))

//getBigImage()

getNewHobbiesForRegisterProfile()